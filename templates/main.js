/* Main code of the GAME!
 * ----------------------
 * @author Julien Deuse
 * 5 february 2014
*/

///********************************************************************************
///**************************** LOADING THE ASSETS ********************************
///**** Chargements de tout ce qui est texture, et le reste....
var loading_finished = false;

//SPRITESHEET textures stocked here
var explosionTextures = [];
var text_fight_msg;
var text_goku, text_goku_jump, text_goku_shoot, text_goku_jump_shoot, text_frieza;
var texture_dragon_ball, texture_frieza_energy;
var texture_hb_empty, texture_hb_green;
var texture_bg_fight;
//var boule_freezer, boule_goku;

// Un callback qui est appel�e une fois que tout les assets sont charg�s.
function onAssetsLoaded(){
    for (var i=0; i < 26; i++){
            var texture = PIXI.Texture.fromFrame("Explosion_Sequence_A " + (i+1) + ".png");
            explosionTextures.push(texture);
    };
    
    text_goku = PIXI.Texture.fromFrame("repos.png");
    text_goku_jump = PIXI.Texture.fromFrame("saut-repos.png");
    text_goku_shoot = PIXI.Texture.fromFrame("shoot.png");
    text_goku_jump_shoot = PIXI.Texture.fromFrame("saut-shoot.png");
    texture_frieza = PIXI.Texture.fromFrame("frieza_red.png");
    //createBoomPool();
    
    text_fight_msg = PIXI.Texture.fromFrame("fight_msg.png");
    texture_dragon_ball = PIXI.Texture.fromFrame("blue-ball.png");
    texture_frieza_energy = PIXI.Texture.fromFrame("purple-ball.png");
    texture_hb_empty = PIXI.Texture.fromFrame("health_bar_empty.png");
    texture_hb_green = PIXI.Texture.fromFrame("health_bar_fill.png");
    texture_bg_fight = PIXI.Texture.fromFrame("background_fight.png");
    
    loading_finished = true;
    gokuLoadRemove(); //On supprime les Sprite de chargement
    gokuStartInit(); //On d�marre la scene start
    
}
//Loading the Explosion Sprite
var assetsToLoader = ["templates/goku_game.json"];
loader = new PIXI.AssetLoader(assetsToLoader, true);
loader.onComplete = onAssetsLoaded //callback
loader.load();

//The texture we have to open before the loading is finished
var text_comic_box = PIXI.Texture.fromImage("templates/comic_box.png");
var text_loading_msg = PIXI.Texture.fromImage("templates/loading_msg.png");
var text_youwin = PIXI.Texture.fromImage("templates/youwin.png");
var text_gameover = PIXI.Texture.fromImage("templates/gameover.png");

///********************************************************************************
///**************************** CREATING THE STAGE ********************************
///****

//create our Goku world
var gokuWorld = new World(780,600, 0xffffff); //320

//!! STARTING THE RENDERING SURFACE
var stage = new PIXI.Stage(gokuWorld.background);
var renderer = PIXI.autoDetectRenderer(gokuWorld.width, gokuWorld.height);
renderer.view.style.display = "block";
document.getElementById("comic_container").appendChild(renderer.view); //The renderer is etched in the DOM (HTML)
// set the canvas width and height to fill the screen: responsive
$(renderer.view).css('width', '100%');
//Hack for the IE vertical scaling
$(window).resize(function(){
    $(renderer.view).height($(renderer.view).width() * gokuWorld.height / gokuWorld.width);
    $("#div_drop_goku").height($("#goku_drop").height()*2.5);
});
////Mettre la bonne dimension au canvas (pour IE phone)
//$(renderer.view).height($(renderer.view).width() * gokuWorld.height / gokuWorld.width);
//$("#div_drop_goku").height(387 * $(renderer.view).width()/780); 
/*window.onload = function() {
    //Mettre la bonne dimension au canvas (pour IE phone)
    $(renderer.view).height($(renderer.view).width() * gokuWorld.height / gokuWorld.width);
    
    //Start loop
    //requestAnimFrame( animate ); //Launch the main function
}*/
var myconsole = new PIXI.Text("fps", {font:"20px Arial", fill:"black"});
myconsole.position.y = gokuWorld.height - 50;
myconsole.position.x = 300;

gokuLoadInit();

///********************************************************************************
///******************************* SCENEMANAGER ***********************************
///**** Ordre de passage des diff�rentes sc�nes
var goku_life = 1000;
var frieza_life = 1200; //1200
var player_score = 0;

var current_time = old_time = new Date();
var fps = 50;
requestAnimFrame(sceneManager);
var start_game = false;
var gameloop = true;

function sceneManager() {
    if (gameloop == true) {
        requestAnimFrame(sceneManager);
    }
    
    if (loading_finished == false)
        gokuLoadLoop();
    else if (start_game == false)
        gokuStartLoop();
    else if (goku_life > 0 && frieza_life > 0 ) {
        goku_game();   
    }else{
        document.getElementById("fb_button").style.display = "block";
        
        if (frieza_life <= 0) {
            youwin.visible = true;
            document.getElementById("score1").innerHTML = player_score;
            document.getElementById("score2").innerHTML = player_score;
            document.getElementById("goku_win").style.display = "block";
            $("#goku_win").animate({opacity: 1.0}, 400);
            
            if (player_score>9000)
                document.getElementById("score_9000").style.display = "block";
            else
                document.getElementById("score").style.display = "block";
        }else if (goku_life <= 0){
            gameover.visible = true;
            document.getElementById("score_loss").style.display = "block";
            document.getElementById("you_fail_img").style.display = "block";
            $("#you_fail_img").animate({opacity: 1.0}, 400);
        }
        //document.getElementById("fb_button").style.display = "block";
        
        //On arrete le jeu
        gameloop = false;
    }


  
    renderer.render(stage);
}


