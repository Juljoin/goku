// GOKU - Setting up his Sprite
var goku = new PIXI.Sprite(texture_goku);
goku.anchor.x = 0.5; //Define anchor point
goku.anchor.y = 0.5;
goku.position.x = 100; //Initial position
goku.position.y = 150;
stage.addChild(goku); // => NOW Goku is in the scene

//FRIEZA - the big boss of death
var frieza = new PIXI.Sprite(texture_frieza);
frieza.anchor.x = 0.5; //Define anchor point
frieza.anchor.y = 0.5;
frieza.position.x = gokuWorld.width - 100; //Initial position
frieza.position.y = gokuWorld.height - 100;
stage.addChild(frieza); // => NOW Goku is in the scene

var o_frieza_ball_x = frieza.position.x - 70;
var o_frieza_ball_y = frieza.position.y + 20;

//INIT POOLS
var pool_goku = new ProjectilePool();
pool_goku.createPool(15, texture_dragon_ball);

var pool_frieza = new ProjectilePool();
pool_frieza.createPool(5, texture_frieza_energy);

var pool_boom = new BoomPool();
function initiate_pool_boom(){
    pool_boom.createPool(7);
}

//Health bar
var initial_scale_hb = 1.3;
var gk_hb_e = new PIXI.Sprite(texture_hb_empty);
var gk_hb_g = new PIXI.Sprite(texture_hb_green);
gk_hb_g.anchor.x = gk_hb_e.anchor.x = 0; //Define anchor point
gk_hb_g.anchor.y = gk_hb_e.anchor.y = 0;
gk_hb_g.position.x = gk_hb_e.position.x = 50; //Initial position
gk_hb_g.position.y = gk_hb_e.position.y = 20;
gk_hb_g.scale.x = gk_hb_e.scale.x = initial_scale_hb; //Initial position
gk_hb_g.scale.y = gk_hb_e.scale.y = initial_scale_hb;
stage.addChild(gk_hb_e); 
stage.addChild(gk_hb_g);
var fz_hb_e = new PIXI.Sprite(texture_hb_empty);
var fz_hb_g = new PIXI.Sprite(texture_hb_green);
fz_hb_g.anchor.x = fz_hb_e.anchor.x = 1; //Define anchor point
fz_hb_g.anchor.y = fz_hb_e.anchor.y = 0;
fz_hb_g.position.x = fz_hb_e.position.x  = gokuWorld.width - 50; //Initial position
fz_hb_g.position.y = fz_hb_e.position.y = 20;
fz_hb_g.scale.x = fz_hb_e.scale.x = initial_scale_hb; //Initial position
fz_hb_g.scale.y = fz_hb_e.scale.y = initial_scale_hb;
stage.addChild(fz_hb_e); 
stage.addChild(fz_hb_g); 

var scoreboard = new PIXI.Text("0", {font:"bold 40px Arial", fill:"#d7b903", stroke: "#252700", strokeThickness:3});
scoreboard.anchor.x = scoreboard.anchor.y = 0.5;
scoreboard.position.x = gokuWorld.width/2;
scoreboard.position.y = 33;
scoreboard.scale.y = 0.8;
stage.addChild(scoreboard);



