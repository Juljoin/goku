///********************************************************************************
///**************************** CHANGES TO PIXI.JS ********************************
///********************************************************************************
///**** On r�ecrit certaines fonctions de PIXI

//Permets de savoir si un Sprite appartient a un PIXI.stage
PIXI.DisplayObjectContainer.prototype.contains = function(child)
{
    return (this.children.indexOf( child ) !== -1);
}

//On enleve les options -ms-touch-start et -ms-content-zooming
PIXI.InteractionManager.prototype.setTargetDomElement = function(domElement)
{
    this.removeEvents();

    this.interactionDOMElement = domElement;
   
    domElement.addEventListener('mousemove',  this.onMouseMove, true);
    domElement.addEventListener('mousedown',  this.onMouseDown, true);
    domElement.addEventListener('mouseout',   this.onMouseOut, true);

    // aint no multi touch just yet!
    domElement.addEventListener('touchstart', this.onTouchStart, true);
    domElement.addEventListener('touchend', this.onTouchEnd, true);
    domElement.addEventListener('touchmove', this.onTouchMove, true);
    
    document.body.addEventListener('mouseup',  this.onMouseUp, true);
};

//On enleve les options -ms-touch-start et -ms-content-zooming
PIXI.InteractionManager.prototype.removeEvents = function()
{
    if(!this.interactionDOMElement)return;
    
    this.interactionDOMElement.removeEventListener('mousemove',  this.onMouseMove, true);
    this.interactionDOMElement.removeEventListener('mousedown',  this.onMouseDown, true);
    this.interactionDOMElement.removeEventListener('mouseout',   this.onMouseOut, true);
    
    // aint no multi touch just yet!
    this.interactionDOMElement.removeEventListener('touchstart', this.onTouchStart, true);
    this.interactionDOMElement.removeEventListener('touchend', this.onTouchEnd, true);
    this.interactionDOMElement.removeEventListener('touchmove', this.onTouchMove, true);

    this.interactionDOMElement = null;

    document.body.removeEventListener('mouseup',  this.onMouseUp, true);
};
