///********************************************************************************
///************************** CREATING THE OBJECTS ********************************
///********************************************************************************
///**** Cr�ations et initialisation de tout les objets globaux.

var goku, frieza;
var o_frieza_ball_x, o_frieza_ball_y;
var pool_goku, pool_frieza, pool_boom;
var initial_scale_hb = 1.3;
var gk_hb_e, gk_hb_g;
var fz_hb_e, fz_hb_g;
var mask_balls;
var scoreboard;
var gameover, youwin;

//ANTI-GARBAGE COLLECTOR
var balls_goku = []; //Array qui va contenir les boules de feux actives
var balls_frieza = [];
var boom_list = []; //Array qui contient les explosions actives

/*see main.js
var goku_life = 1000;
var frieza_life = 1200;
var player_score = 0;*/
var goku_dmg = 20;
var frieza_dmg = 125;
var shoot_duration = 200; //In milliseconds, only used for the animation
var last_shoot = new Date();
last_shoot.setTime(0); //Il y a 44 ans

///********************************************************************************
///************************ INITIALISING THE PHYSICS ******************************
///**** Initialisation de notre moteur physique
//Goku's acceleration and velocity vector
var v_goku = new Vector(0,0); // (0,2)
var a_goku = new Vector(0,0.4); //0.3
var goku_ball_v = 7;
var balls_frieza_v = 10; //17
var goku_jump_speed = -12;
var next_attack = new Date(); //Attaques de frieza

///********************************************************************************
///*************************** USER INTERACTION  **********************************
///**** Gestion de l'interaction de l'utilisateur
var key_shoot_released = true;
var key_jump_released = true;
var goku_tapshot = false;

///********************************************************************************
///******************************* FUNCTIONS **************************************
function playAgain(){
    /*var destination = $("#image3").offset().top + $("#image3").height();
    $("html, body").animate({ scrollTop: destination - 15}, 500 );*/
    
    setTimeout(resetGameValues, 500)
}

function resetGameValues(){
    goku_life = 1000;
    frieza_life = 1200; //1200
    player_score = 0;

    gameloop = true;
    requestAnimFrame(sceneManager);
    
    //Vider les boules
    var i = balls_frieza.length; //Il a deja �t� d�clar� plus tot
    while(i--){
        balls_frieza[i].visible = false;
        pool_frieza.returnProj(balls_frieza.splice(i, 1)[0]);
    }
    i = balls_goku.length; //Il a deja �t� d�clar� plus tot
    while(i--){
        balls_goku[i].visible = false;
        pool_goku.returnProj(balls_goku.splice(i, 1)[0]);
    }
    
    gameover.visible = false;
    youwin.visible = false;
    
    gk_hb_g.visible = true;
    fz_hb_g.visible = true;
    
    //Pour que la premiere attaque de frieza ne oit pas instantann�
    next_attack.setTime(current_time.valueOf() + 700);
    
    document.getElementById("goku_win").style.display = "none";
    document.getElementById("score_9000").style.display = "none";
    document.getElementById("score").style.display = "none";
    document.getElementById("you_fail_img").style.display = "none";
    //document.getElementById("fb_button").style.display = "none";
    document.getElementById("you_fail_img").style.opacity = 0;
    document.getElementById("goku_win").style.opacity = 0;
    
}

function loadPNJ(){
    bg_fight = new PIXI.Sprite(texture_bg_fight);
    bg_fight.anchor.x = bg_fight.anchor.y = 0;
    bg_fight.position.x = 30; //Initial position
    bg_fight.position.y = 2;
    //stage.addChild(bg_fight);
    
    mask_balls = new PIXI.Graphics();
    mask_balls.beginFill();
    mask_balls.drawRect(30, 2, 716, 592);
    mask_balls.endFill();
    stage.addChild(mask_balls)
    
    // GOKU - Setting up his Sprite
    goku = new PIXI.Sprite(text_goku_jump);
    goku.anchor.x = goku.anchor.y = 0.5;
    goku.position.x = 100; //Initial position
    goku.position.y = 150;
    goku.mask = mask_balls;
    goku_nor = new PIXI.Sprite(text_goku); //Goku au repos
    goku_nor.anchor.x = goku_nor.anchor.y = 0.5;
    goku_nor.position.x = goku.position.x;
    goku_nor.position.y = goku.position.y;
    goku_nor.visible = false;
    goku_shoot = new PIXI.Sprite(text_goku_shoot);
    goku_shoot.anchor.x = goku_shoot.anchor.y = 0.5;
    goku_shoot.position.x = goku.position.x;
    goku_shoot.position.y = goku.position.y;
    goku_shoot.visible = false;
    goku_jumpshoot = new PIXI.Sprite(text_goku_jump_shoot);
    goku_jumpshoot.anchor.x = goku_jumpshoot.anchor.y = 0.5;
    goku_jumpshoot.position.x = goku.position.x;
    goku_jumpshoot.position.y = goku.position.y;
    goku_jumpshoot.visible = false;
    goku_jumpshoot.mask = mask_balls;
    stage.addChild(goku); // => NOW Goku is in the scene
    stage.addChild(goku_nor);
    stage.addChild(goku_shoot);
    stage.addChild(goku_jumpshoot);
    
    //FRIEZA - the big boss of death
    frieza = new PIXI.Sprite(texture_frieza);
    frieza.anchor.x = 0.5; //Define anchor point
    frieza.anchor.y = 0.5;
    frieza.scale.x = 0.9;
    frieza.scale.y = 0.9;
    frieza.position.x = 654;//gokuWorld.width - 100; //Initial position
    frieza.position.y = 215;// 204 gokuWorld.height - 90;
    stage.addChild(frieza); // => NOW Goku is in the scene
    
    o_frieza_ball_x = 610; //frieza.position.x - 70;
    o_frieza_ball_y = 180;// 190 frieza.position.y - 20;
    
    //INIT POOLS
    pool_goku = new ProjectilePool();
    pool_goku.createPool(15, texture_dragon_ball, mask_balls);
    
    pool_frieza = new ProjectilePool();
    pool_frieza.createPool(5, texture_frieza_energy, mask_balls);
    
    pool_boom = new BoomPool();
    pool_boom.createPool(7);
    
    
    //Health bar
    gk_hb_e = new PIXI.Sprite(texture_hb_empty);
    gk_hb_g = new PIXI.Sprite(texture_hb_green);
    gk_hb_g.anchor.x = gk_hb_e.anchor.x = 0; //Define anchor point
    gk_hb_g.anchor.y = gk_hb_e.anchor.y = 0;
    gk_hb_g.position.x = gk_hb_e.position.x = 50; //Initial position
    gk_hb_g.position.y = gk_hb_e.position.y = 20;
    gk_hb_g.scale.x = gk_hb_e.scale.x = initial_scale_hb; //Initial position
    gk_hb_g.scale.y = gk_hb_e.scale.y = initial_scale_hb;
    stage.addChild(gk_hb_e); 
    stage.addChild(gk_hb_g);
    fz_hb_e = new PIXI.Sprite(texture_hb_empty);
    fz_hb_g = new PIXI.Sprite(texture_hb_green);
    fz_hb_g.anchor.x = fz_hb_e.anchor.x = 1; //Define anchor point
    fz_hb_g.anchor.y = fz_hb_e.anchor.y = 0;
    fz_hb_g.position.x = fz_hb_e.position.x  = gokuWorld.width - 50; //Initial position
    fz_hb_g.position.y = fz_hb_e.position.y = 20;
    fz_hb_g.scale.x = fz_hb_e.scale.x = initial_scale_hb; //Initial position
    fz_hb_g.scale.y = fz_hb_e.scale.y = initial_scale_hb;
    stage.addChild(fz_hb_e); 
    stage.addChild(fz_hb_g); 
    
    scoreboard = new PIXI.Text("0", {font:"bold 40px Arial", fill:"#d7b903", stroke: "#252700", strokeThickness:3});
    scoreboard.anchor.x = scoreboard.anchor.y = 0.5;
    scoreboard.position.x = gokuWorld.width/2;
    scoreboard.position.y = 33;
    scoreboard.scale.y = 0.8;
    stage.addChild(scoreboard);
    // Add the console
    stage.addChild(myconsole);
    
    gameover = new PIXI.Sprite(text_gameover);
    gameover.anchor.x = gameover.anchor.y = 0.5; 
    gameover.position.x = gokuWorld.width/2; //Initial position
    gameover.position.y = gokuWorld.height/2;
    gameover.visible = false;
    youwin = new PIXI.Sprite(text_youwin);
    youwin.anchor.x = youwin.anchor.y = 0.5; 
    youwin.position.x = gokuWorld.width/2; //Initial position
    youwin.position.y = gokuWorld.height/2;
    youwin.visible = false;
    stage.addChild(gameover);
    stage.addChild(youwin);
}

function setUserInteraction(){
    goku.setInteractive(true);
    frieza.setInteractive(true);
      
    goku.mousedown = function(){
        if(v_goku.y==0){v_goku.y = goku_jump_speed};
    }
    goku.touchstart = function(){
        if(v_goku.y==0){v_goku.y = goku_jump_speed};
    }
    /*key("up", function(){if(v_goku.y==0){v_goku.y = goku_jump_speed}});
    key("space", function(){if(v_goku.y==0){v_goku.y = goku_jump_speed}});*/
    frieza.mousedown = function(){
        goku_tapshot = true;
    }
    frieza.touchstart = function(){
        goku_tapshot = true;
    }
    
    //Pour que la premiere attaque de frieza ne oit pas instantann�
    next_attack.setTime(current_time.valueOf() + 700);
}

function goku_game() {
    //requestAnimFrame( goku_game ); //Au lieu de animate() => pour cr�er une boucle avec un delay (max fps).
 
    //fps ainsi que temps pour les boules de frieza.
    old_time = current_time;
    current_time = new Date();
    var delta_t = current_time.valueOf() - old_time.valueOf();

    fps = Math.floor(0.95*fps + 0.05*1000/(delta_t));
    scoreboard.setText(Math.floor(player_score));
    myconsole.setText("fps: " + fps);
    
    ///********************************************************************************
    ///*************************** USER INTERACTION  **********************************
    ///**** Gestion de l'interaction de l'utilisateur
    //JUMP
    if ( key_jump_released == true && v_goku.y==0 && (key.isPressed("space") || key.isPressed("up"))) {
        v_goku.y = goku_jump_speed; //-11
            
        key_jump_released = false;
    }
    if (key.isPressed("space") == false && key.isPressed("up") == false) {
        key_jump_released = true;
    }

    //SHOOT
    if (key_shoot_released == true && key.isPressed("right")) {
        key_shoot_released = false;
        var temp_ball = pool_goku.borrowProj(goku.position.x + 70, goku.position.y - 15); //Starting pos of the ball
        last_shoot = new Date(); //for the shooting animation
        
        balls_goku.push(temp_ball);
        //stage.addChild(temp_ball);
        temp_ball.visible = true;
    }
    if(!key.isPressed("right")){
        key_shoot_released = true;
    }
    
    if (goku_tapshot == true){
        goku_tapshot = false;
        var temp_ball = pool_goku.borrowProj(goku.position.x + 70, goku.position.y - 15);
        last_shoot = new Date(); //for the shooting animation
        
        balls_goku.push(temp_ball);
        //stage.addChild(temp_ball);
        temp_ball.visible = true;
    }
    
    ///********************************************************************************
    ///***************************** UPDATING PHYSICS *********************************
    ///**** Updates de notre moteur physique
        
    //SANGOKU
    //-------
    var corr_pos = new Vector(0,0);
    corr_pos.y = goku.position.y + goku.height/2; //at Goku's feet
    corr_pos.x = goku.position.x;
    
    //New velocity -> y axis
    if (corr_pos.y <= gokuWorld.height) {
        //see http://www.niksula.hut.fi/~hkankaan/Homepages/gravity.html for an explanation of why
        v_goku.y += 0.5*a_goku.y*delta_t/17;
        corr_pos.y += v_goku.y*delta_t/17;
        v_goku.y += 0.5*a_goku.y*delta_t/17;
    }//Velocity x-axis = 0
    
    //Boundary constraints Goku
    //y axis
    if (corr_pos.y > gokuWorld.height){
        corr_pos.y = gokuWorld.height;
        v_goku.y = 0; // Bunny hit the ground => v_y = 0
    }
    
    goku.position.y = corr_pos.y - goku.height/2;
    //--------
    //END GOKU
    
    ///********************************************************************************
    ///******************************** ANIMATION *************************************

    if (goku.position.y + goku.height/2 < gokuWorld.height) {
        //Jump
        if(current_time.valueOf() - last_shoot.valueOf() < shoot_duration){
            goku.alpha = 0;
            goku_jumpshoot.visible = true;
        }else{
            goku.alpha = 1;
            goku_jumpshoot.visible = false;
        }
        goku_nor.visible = false;
        goku_shoot.visible = false;
    }else{
        if(current_time.valueOf() - last_shoot.valueOf() < shoot_duration){
            goku_nor.visible = false;
            goku_shoot.visible = true;
        }else{
            goku_nor.visible = true;
            goku_shoot.visible = false;
        }       
        goku.alpha = 0;
        goku_jumpshoot.visible = false;    
    }
    //Updating the position of all 3 goku sprites
    goku_nor.position.y = goku.position.y;
    goku_shoot.position.y = goku.position.y;
    goku_jumpshoot.position.y = goku.position.y;
    
    //ENERGY BALLS - GOKU
    //------------
    //Parcourir la liste de energy balls (balls_goku[])
    var i = balls_goku.length;
    while(i--){
        var remove = false;
        balls_goku[i].position.x += goku_ball_v*delta_t/17;
        
        //On supprime si la balle quitte la map
        if (balls_goku[i].position.x > gokuWorld.width + balls_goku[i].width/2) {
            // stage.removeChild(balls_goku[i]);
            balls_goku[i].visible = false;
            remove = true;
        }else if ( balls_goku[i].position.x + balls_goku[i].width/2 > frieza.position.x - frieza.width/2 + 45 &&
                  balls_goku[i].position.y + balls_goku[i].height/2 > frieza.position.y - frieza.height/2 ){
            //stage.removeChild(balls_goku[i]);
            balls_goku[i].visible = false;
            
            var boom = pool_boom.borrowBoom(balls_goku[i].position.x, balls_goku[i].position.y, 1);
            //stage.addChild(boom);
            boom.visible = true;
            boom.play();
            boom_list.push(boom);
            
            //FRIEZA A MAL
            frieza_life -= goku_dmg;
            //On augmente le player score (plus on est bless�, moins �a va)
            if (goku_life > 0)
                player_score += 80* goku_dmg/5 * (goku_life/1000 + 0.3)/2; //Le 80 c'est pour arriver au over 9000
            
            
            //Garbage collect the ball:
            remove = true;
        }
        
        //Put it back in the pool       
        if (remove == true) {
            pool_goku.returnProj(balls_goku.splice(i, 1)[0]);
        }
    }
   
    //FREEZER ENERGY BALL
    //-------------------    
    //Attack
    // The attack patern has to be made so you always can evade the frieza balls.
    // Goku tombe
    var goku_speedo;
    var d_goku_frieza = frieza.position.x - goku.position.x - goku.width/2 - frieza.width/2; //On a agrendi l'intervalle pour plus de challenge

    if (v_goku.y == 0)
        goku_speedo = (gokuWorld.height - o_frieza_ball_y)/(Math.abs(goku_jump_speed/2));
    else
        goku_speedo = (gokuWorld.height - goku.position.y - goku.height/2)/(Math.abs(v_goku.y/2)) + (gokuWorld.height - o_frieza_ball_y)/(Math.abs(goku_jump_speed/2));

   if (v_goku.y >=0 && current_time > next_attack && goku_speedo < d_goku_frieza/Math.abs(balls_frieza_v)) { //current_time > next_attack &&
        var temp_ball = pool_frieza.borrowProj(o_frieza_ball_x, o_frieza_ball_y);

        balls_frieza.push(temp_ball);
        temp_ball.visible = true;
        //stage.addChild(temp_ball);
        next_attack.setTime(current_time.valueOf() + Math.random()*1500);
    }
    
    //Parcourir la liste des frieza balls (balls_frieza[])
    i = balls_frieza.length; //Il a deja �t� d�clar� plus tot
    while(i--){
        var remove = false;
        balls_frieza[i].position.x -= balls_frieza_v*delta_t/17;
        
        //On supprime si la balle quitte la map
        if (balls_frieza[i].position.x < 0 - balls_frieza[i].width/2) { 
            //stage.removeChild(balls_frieza[i]);
            balls_frieza[i].visible = false;
                        
            remove = true;
        }else if (balls_frieza[i].position.x - balls_frieza[i].width/2 < goku.position.x + goku.width/2 && balls_frieza[i].position.x + balls_frieza[i].width/2 > goku.position.x - goku.width/2 && balls_frieza[i].position.y - balls_frieza[i].height/2 < goku.position.y + goku.height/2){
            //stage.removeChild(balls_frieza[i]);
            balls_frieza[i].visible = false;
          
            var boom = pool_boom.borrowBoom(balls_frieza[i].position.x, balls_frieza[i].position.y, 0.5);
            //stage.addChild(boom);
            boom.visible = true;
            boom.play();
            boom_list.push(boom);
            
            
            //GOKU A MAL
            goku_life -= frieza_dmg;
            
            //Garbage collect the ball:
            remove = true;
        }
        
        //Garbage collect the ball:
        if (remove == true) {
            pool_frieza.returnProj(balls_frieza.splice(i, 1)[0]);
        }
    }
    
    //EXPLOSIONS
    //-------------------
    //Parcourir la liste des explosions
    i = boom_list.length;
    while(i--) {
        if (boom_list[i].playing == false)
        {
            //stage.removeChild(boom_list[i]);
            boom_list[i].visible = false;
            pool_boom.returnBoom(boom_list.splice(i , 1)[0]);
        }else{
            boom_list[i].animationSpeed = 1 * delta_t / 17;
        }
    }
    
    ///********************************************************************************
    ///******************************* HEALTH BAR *************************************
    if (goku_life>0)
        gk_hb_g.scale.x = initial_scale_hb * (goku_life/1000);
    else
        gk_hb_g.visible = false;

    if (frieza_life>0) 
        fz_hb_g.scale.x = initial_scale_hb * (frieza_life/1200);
    else
        fz_hb_g.visible = false;
        
    // render the stage  
    //renderer.render(stage);
}












