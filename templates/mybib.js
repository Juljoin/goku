///********************************************************************************
///***************************** MY LIBRARY ***************************************
///********************************************************************************
///**** Déclaration des fonctions auxiliaires

Vector = PIXI.Point;

//Custom copy a Sprite
function copySprite(osprite){
    var temp = new PIXI.Sprite(osprite.texture); //give temp the sprite constructor
    
    temp.anchor.x = osprite.anchor.x;
    temp.anchor.y = osprite.anchor.y;
    temp.position.x = osprite.position.x;
    temp.position.y = osprite.position.y;
    temp.mask = osprite.mask;
    
    return temp;
}

// WORLD OBJECT: Defines the size of the world, the background colour, and other world related caracteristics
function World(max_x, max_y, colour) {
    this.width = max_x;
    this.height = max_y;
    this.background = colour;
    this.gravity = 2;
}

//Create BOOOM
function create_boom(){
    var temp = new PIXI.MovieClip(explosionTextures);
    
    temp.anchor.x = 0.5;
    temp.anchor.y = 0.5;
    temp.loop = false;
    temp.visible = false;
    stage.addChild(temp);
    
    return temp; //Add this to the anti-garbage collector
}