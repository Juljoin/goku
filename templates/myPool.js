///********************************************************************************
///******************************* MY POOL ****************************************
///********************************************************************************
///**** Cr�ation des objets pool.

////BOOOOOOOOOOOOOOOM POOOOOOOOOOOOOL
function BoomPool() {
    this.pool = new Array();
}
BoomPool.prototype.createPool = function(size){
    for (var i=0; i < size; i++){
        this.addBoom();
    }
};
BoomPool.prototype.addBoom = function(){
    var boom = create_boom();
    
    this.pool.push(boom);
};
BoomPool.prototype.borrowBoom = function(new_pos_x, new_pos_y, scale){
    if (this.pool.length == 1) {
        this.pool.push(create_boom()); //duplicate if only one remaining item
        console.log("Size of the pool increased by one. (boom)");
    }
    var temp_boom = this.pool.pop(); //remove last item of the array
    
    temp_boom.position.x = new_pos_x;
    temp_boom.position.y = new_pos_y;
    temp_boom.scale.x = scale;
    temp_boom.scale.y = scale;
    
    return temp_boom;
};
BoomPool.prototype.returnBoom = function(boom){
    boom.stop();
    boom.currentFrame = 0;
    
    this.pool.push(boom);
};

// PROJECTILE POOOOOOOOOOOL
function ProjectilePool() {
    this.pool = new Array();
}
ProjectilePool.prototype.createPool = function(size, projText, mymask){
    for (var i=0; i < size; i++){
        this.addProjectile(projText, mymask);
    }
};
ProjectilePool.prototype.addProjectile = function(projText, mymask){
    var proj = new PIXI.Sprite(projText);
    
    proj.anchor.x = 0.5; //Define anchor point
    proj.anchor.y = 0.5;
    proj.mask = mymask;
    proj.visible = false;
    stage.addChild(proj);
    
    this.pool.push(proj);
};
ProjectilePool.prototype.borrowProj = function(new_pos_x, new_pos_y){
    if (this.pool.length == 1) {
        var proj = copySprite(this.pool[0]);
        proj.visible = false;
        stage.addChild(proj);
        
        this.pool.push(copySprite(proj)); //duplicate if only one remaining item
        console.log("Size of the pool increased by one. (proj)");
    }
    var temp_sprite = this.pool.pop(); //remove last item of the array
    
    temp_sprite.position.x = new_pos_x;
    temp_sprite.position.y = new_pos_y;

    return temp_sprite;
};
ProjectilePool.prototype.returnProj = function(projectile){
    this.pool.push(projectile);
};
//read array
ProjectilePool.prototype.readPool = function(){
    console.log("length: " + this.pool.length);
    
    var i = this.pool.length;
    
    while (i--) {
        if (this.pool[i] instanceof PIXI.Sprite)
            console.log("element " + i + " is a sprite");
        else
            console.log("element " + i + " is not a sprite!");
    }
}