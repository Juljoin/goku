
var loadingmsg;
var startmsg;
var comic_box;

///********************************************************************************
///****************************** LOADING SCREEN **********************************
function gokuLoadInit() {    
    comic_box = new PIXI.Sprite(text_comic_box);
    comic_box.anchor.x = comic_box.anchor.y = 0;
    comic_box.position.x = 0; //Initial position
    comic_box.position.y = 0;
    //stage.addChild(comic_box);

    loading_msg = new PIXI.Sprite(text_loading_msg);
    loading_msg.anchor.x = loading_msg.anchor.y = 0.5;
    loading_msg.position.x = gokuWorld.width/2; //Initial position
    loading_msg.position.y = gokuWorld.height/2;
    stage.addChild(loading_msg);
    
    // Our console
    stage.addChild(myconsole);
}
function gokuLoadRemove(){
    stage.removeChild(loading_msg);
}

function gokuLoadLoop() {
    //Impression des fps
    old_time = current_time;
    current_time = new Date()   
    fps = Math.floor(0.95*fps + 0.05*1000/(current_time.valueOf() - old_time.valueOf()));
    myconsole.setText("fps: " + fps);
}
///********************************************************************************
///****************************** WAITING SCREEN **********************************

function gokuStartInit() {
    //startmsg = new PIXI.Text("Start", {font:"bold 80px Arial", fill:"#d7b903", stroke: "#252700", strokeThickness:3});
    startmsg = new PIXI.Sprite(text_fight_msg);
    startmsg.anchor.x = startmsg.anchor.y = 0.5;
    startmsg.position.x = gokuWorld.width/2;
    startmsg.position.y = gokuWorld.height/2;
    startmsg.setInteractive(true);

    
    startmsg.click = function(){
        start_game = true;
        gokuStartRemove();
        loadPNJ();
        setUserInteraction();
    }
    startmsg.tap = function(){
        start_game = true;
        gokuStartRemove();
        loadPNJ();
        setUserInteraction();
    }
    
    stage.addChild(startmsg);
}

function gokuStartRemove(){
    stage.removeChild(startmsg);
    stage.removeChild(myconsole);
}

function gokuStartLoop() {    
    //Impression des fps
    old_time = current_time;
    current_time = new Date()   
    fps = Math.floor(0.95*fps + 0.05*1000/(current_time.valueOf() - old_time.valueOf()));
    myconsole.setText("fps: " + fps);
}




